Local Weather App
=================

Test project developed for **STRV** during the recruitment. Project architecture is inspired (some parts taken - e.g. baseViewModel, baseFragment...) from experimental Android app [Stocks](https://github.com/petrnohejl/Android-Stocks) by [Petr Nohejl](http://petrnohejl.cz/) and uses [Alfonz](https://github.com/petrnohejl/Alfonz) library (mainly MVVM part which encapsulates [AndroidViewModel](https://github.com/inloop/AndroidViewModel) library - developed by [inloop](https://www.inloop.eu/)). Assets were provided by STRV, Launcher icon, menu and settings icons were generated either using [Android Asset Stuido](http://romannurik.github.io/AndroidAssetStudio/) or Android Vector Asset within the Android Studio.

Build
-----

The project can be build using **Android Studio 2.3** and **buildToolsVersion "25.0.2"**, the **target SDK version** is **25** (corresponding to **Android 7.1.1** Nougat), **minimal SDK version** is **16** (**Android 4.1** Jelly Bean). Project has following dependencies.

Dependencies
------------

* [Compat Support Libraries](https://developer.android.com/topic/libraries/support-library/index.html) (Vector Drawables, Constraint Layout, RecyclerView, GridLayout, AppCompat, Design)
* [Google Play Services](https://developers.google.com/android/reference/com/google/android/gms/location/package-summary) (Location)
* [Alfonz](https://github.com/petrnohejl/Alfonz) (MVVM, View, Graphics, Adapter, Utility)
* [Retrofit2](https://github.com/square/retrofit) (Retrofit, Converter-gson)
* [GSON](https://github.com/google/gson)
* [Glide](https://github.com/bumptech/glide)

REST API
--------

Images ([example](http://openweathermap.org/img/w/10d.png))

* [http://openweathermap.org/img/w/](http://openweathermap.org/img/w/)

Current Weather ([example](http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b1b15e88fa797225412429c1c50c122a1))

* [http://samples.openweathermap.org/data/2.5/weather](http://samples.openweathermap.org/data/2.5/weather)

Daily Forecast ([example](http://samples.openweathermap.org/data/2.5/forecast/daily?lat=35&lon=139&cnt=10&appid=b1b15e88fa797225412429c1c50c122a1))

* [http://samples.openweathermap.org/data/2.5/forecast/daily](http://samples.openweathermap.org/data/2.5/forecast/daily)


More at [OpenWeatherMap API](http://openweathermap.org/api)

TODO
----

* Preserve ViewModel on screen reorientation
* Override StatefulLayout animation
* Figure out proper unit conversion
* Implement [RxJava](https://github.com/ReactiveX/RxJava)

Developed by
------------

[Martin Rončka](https://www.linkedin.com/in/martin-ron%C4%8Dka-b1bb7680/)