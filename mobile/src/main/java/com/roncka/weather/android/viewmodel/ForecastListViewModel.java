package com.roncka.weather.android.viewmodel;

import android.content.Context;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;

import com.roncka.weather.android.LocalWeatherApplication;
import com.roncka.weather.android.LocalWeatherConfig;
import com.roncka.weather.android.entity.ForecastEntity;
import com.roncka.weather.android.rest.dto.DailyForecastListDTO;
import com.roncka.weather.android.rest.dto.ForecastDTO;
import com.roncka.weather.android.rest.provider.ForecastServiceProvider;
import com.roncka.weather.android.service.LocationService;
import com.roncka.weather.android.view.ForecastListView;

import org.alfonz.utility.NetworkUtility;
import org.alfonz.view.StatefulLayout;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Author:          Harry
 * Date:   			3/24/2017
 * Inspiration: 	https://github.com/petrnohejl/Android-Stocks/blob/master/mobile/src/main/java/com/example/viewmodel/StockListViewModel.java
 */
public class ForecastListViewModel extends BaseViewModel<ForecastListView> {

    public final ObservableField<StatefulLayout.State> state = new ObservableField<>();
    public final ObservableArrayList<ForecastEntity> forecastList = new ObservableArrayList<>();

    private int TemperatureUnit = com.roncka.weather.android.utility.TemperatureUnit.CELSIUS_CODE;


    @Override
    public void onStart() {
        super.onStart();

        if (forecastList.isEmpty())
            loadData();
        else
            ensureUnits();
    }


    @Override
    public void updateViewModel() {
        loadData();
    }


    private void ensureUnits() {
        if (TemperatureUnit != com.roncka.weather.android.utility.TemperatureUnit.getUnit()) {
            TemperatureUnit = com.roncka.weather.android.utility.TemperatureUnit.getUnit();

            for (ForecastEntity forecast : forecastList) {
                setUnitDependentValues(forecast);
            }

            synchronized (forecastList) {
                forecastList.notifyAll();
            }
        }
    }


    private void loadData() {
        Context context = LocalWeatherApplication.getContext();
        state.set(StatefulLayout.State.PROGRESS);

        if (NetworkUtility.isOnline(LocalWeatherApplication.getContext())) {

            Call<DailyForecastListDTO> today;

            if (context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE).getBoolean(LocationService.KEY_LOCATION_KNOWN, false)) {
                float latitude = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE).getFloat(LocationService.KEY_LATITUDE, 0);
                float longitude = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE).getFloat(LocationService.KEY_LONGITUDE, 0);
                today = ForecastServiceProvider.getService().forecast(latitude, longitude);
            } else {
                today = ForecastServiceProvider.getService().forecast("Brno");
            }

            today.enqueue(new Callback<DailyForecastListDTO>() {
                @Override
                public void onResponse(Call<DailyForecastListDTO> call, Response<DailyForecastListDTO> response) {

                    for (ForecastDTO weather : response.body().getList()) {
                        forecastList.add(createForecastModel(weather));
                    }

                    state.set(StatefulLayout.State.CONTENT);
                }

                @Override
                public void onFailure(Call<DailyForecastListDTO> call, Throwable t) {
                    state.set(StatefulLayout.State.OFFLINE);
                }
            });
        } else {
            state.set(StatefulLayout.State.OFFLINE);
        }
    }


    private ForecastEntity createForecastModel(ForecastDTO weather) {

        ForecastEntity forecast = new ForecastEntity();

        String description = weather.getDescription();
        String forecastDescription = description.substring(0, 1).toUpperCase() + description.substring(1);
        Date date = new Date(weather.getDate() * 1000);

        forecast.setDescription(String.format("%s on %s", forecastDescription, new SimpleDateFormat("EEEE", Locale.getDefault()).format(date)));
        forecast.setIconUrl(String.format("%s%s.png", LocalWeatherConfig.REST_BASE_RESOURCES_URL, weather.getIconName()));
        forecast.setTemperatureData(weather.getTemperature());

        setUnitDependentValues(forecast);

        return forecast;
    }


    private void setUnitDependentValues(ForecastEntity forecast) {
        forecast.setTemperature(com.roncka.weather.android.utility.TemperatureUnit.getInUnit(forecast.getTemperatureData()));
    }
}