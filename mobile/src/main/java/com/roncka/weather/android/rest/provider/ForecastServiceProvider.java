package com.roncka.weather.android.rest.provider;

import com.roncka.weather.android.LocalWeatherConfig;
import com.roncka.weather.android.rest.dto.DailyForecastListDTO;
import com.roncka.weather.android.rest.RetrofitClient;
import com.roncka.weather.android.rest.dto.CurrentWeatherDTO;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Author:          Harry
 * Date:   			3/24/2017
 * Inspiration: 	https://github.com/petrnohejl/Android-Stocks/blob/master/mobile/src/main/java/com/example/rest/provider/StocksServiceProvider.java
 */
public class ForecastServiceProvider {

	private static volatile ForecastService sService;


	// TODO: should move propagate appId to viewModel and pass as parameter?
	@SuppressWarnings("WeakerAccess")
	public interface ForecastService {

		@GET("weather?" + "appid=" + LocalWeatherConfig.API_KEY)
		Call<CurrentWeatherDTO> weather(@Query("q") String city);

		@GET("weather?" + "appid=" + LocalWeatherConfig.API_KEY)
		Call<CurrentWeatherDTO> weather(@Query("lat") float latitude, @Query("lon") float longitude);

		@GET("/data/2.5/forecast/daily?" + "appid=" + LocalWeatherConfig.API_KEY)
		Call<DailyForecastListDTO> forecast(@Query("q") String city);

		@GET("/data/2.5/forecast/daily?" + "appid=" + LocalWeatherConfig.API_KEY)
		Call<DailyForecastListDTO> forecast(@Query("lat") float latitude, @Query("lon") float longitude);
	}


	private ForecastServiceProvider() {
	}


	public static ForecastService getService() {
		if (sService == null) {
			synchronized (ForecastServiceProvider.class) {
				if (sService == null) {
					sService = RetrofitClient.createService(ForecastService.class);
				}
			}
		}
		return sService;
	}
}
