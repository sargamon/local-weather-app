package com.roncka.weather.android.viewmodel;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.roncka.weather.android.view.BaseView;

import org.alfonz.mvvm.AlfonzViewModel;


/**
 * Author:          Harry
 * Date:   			3/24/2017
 * Source:		 	https://github.com/petrnohejl/Android-Stocks/blob/master/mobile/src/main/java/com/example/viewmodel/BaseViewModel.java
 */
@SuppressWarnings("unused")
public abstract class BaseViewModel<T extends BaseView> extends AlfonzViewModel<T> {

	@Override
	public void onCreate(@Nullable Bundle arguments, @Nullable Bundle savedInstanceState) {
		super.onCreate(arguments, savedInstanceState);
	}


	@Override
	public void onBindView(@NonNull T view) {
		super.onBindView(view);
	}


	@Override
	public void onStart() {
		super.onStart();
	}


	@Override
	public void onSaveInstanceState(@NonNull Bundle bundle) {
		super.onSaveInstanceState(bundle);
	}


	@Override
	public void onStop() {
		super.onStop();
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
	}


	public void handleError(String message) {
		if (getView() != null) {
			getView().showToast(message);
		}
	}


	public abstract void updateViewModel();
}
