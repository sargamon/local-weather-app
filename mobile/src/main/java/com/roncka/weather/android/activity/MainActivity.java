package com.roncka.weather.android.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.location.LocationListener;
import com.roncka.weather.android.R;
import com.roncka.weather.android.activity.settings.SettingsActivity;
import com.roncka.weather.android.fragment.BaseFragment;
import com.roncka.weather.android.fragment.ForecastListFragment;
import com.roncka.weather.android.fragment.TodayFragment;
import com.roncka.weather.android.service.LocationService;
import com.roncka.weather.android.viewmodel.BaseViewModel;

import static com.roncka.weather.android.service.LocationService.KEY_LOCATION_TIME;


/**
 * Author:          Harry
 * Date:            3/24/2017
 * Inspiration: 	https://github.com/petrnohejl/Android-Stocks/blob/master/mobile/src/main/java/com/example/activity/StockListActivity.java
 */
@SuppressWarnings("unused")
public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, LocationListener {

    private static final int ACCESS_LOCATION_REQUEST_CODE = 1;

    private static final String KEY_ACTIVE_FRAGMENT = "active-fragment";
    private static final int ACTIVE_FRAGMENT_NONE = -1;
    private static final int ACTIVE_FRAGMENT_TODAY = 0;
    private static final int ACTIVE_FRAGMENT_FORECAST = 1;

    private static final long TIME_TO_REFRESH = 1000 * 60 * 60;

    private SwipeRefreshLayout swipeRefreshLayout;
    private BaseFragment activeFragment;
    private int activeFragmentIndex = ACTIVE_FRAGMENT_NONE;


    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setColorSchemeColors(Color.GRAY);
        swipeRefreshLayout.setEnabled(false);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        int activeFragment = ACTIVE_FRAGMENT_TODAY;
        if (savedInstanceState != null)
            activeFragment = savedInstanceState.getInt(KEY_ACTIVE_FRAGMENT, ACTIVE_FRAGMENT_TODAY);

        switch (activeFragment) {
            case ACTIVE_FRAGMENT_FORECAST:
                showForecastFragment();
                navigationView.getMenu().getItem(1).setChecked(true);
                break;
            default:
                showTodayFragment();
                navigationView.getMenu().getItem(0).setChecked(true);
                break;
        }

        long currentTime = System.currentTimeMillis();

        long lastUpdate = getSharedPreferences(getPackageName(), MODE_PRIVATE).getLong(KEY_LOCATION_TIME, 0);
        if ((currentTime - lastUpdate) > TIME_TO_REFRESH)
            startLocationUpdates();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(KEY_ACTIVE_FRAGMENT, activeFragmentIndex);
        super.onSaveInstanceState(outState);
    }


    public void startLocationUpdates() {
        if (LocationService.checkPermissions(this)) {
            swipeRefreshLayout.setRefreshing(true);
            LocationService.addLocationListener(this, this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }, 4000);
        }
        else
            LocationService.requestPermissions(this, ACCESS_LOCATION_REQUEST_CODE);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        LocationService.removeLocationListener(this, this);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == ACCESS_LOCATION_REQUEST_CODE) {
            if (permissions.length >= 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startLocationUpdates();
            } else {
                showSnackbarPrompt();
            }
        }
    }


    private void showSnackbarPrompt() {
        View view = findViewById(R.id.coordinator_layout);
        Snackbar.make(view, R.string.snackbar_request_location, Snackbar.LENGTH_LONG)
                .setDuration(6000)
                .setAction("ENABLE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startLocationUpdates();
                    }
                }).show();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_refresh) {
            startLocationUpdates();
            return true;
        } else if (id == R.id.action_settings) {
            startActivity(SettingsActivity.newIntent(this));
            return true;
        } else if (id == R.id.action_about) {
            showAboutDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_today) {
            showTodayFragment();
        } else if (id == R.id.nav_forecast) {
            showForecastFragment();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void showAboutDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.dialog_title_about)
                .setMessage(R.string.dialog_message_about)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }


    private void showForecastFragment() {
        if (activeFragmentIndex == ACTIVE_FRAGMENT_FORECAST)
            return;

        setActiveFragmentIndex(ForecastListFragment.newInstance());
        activeFragmentIndex = ACTIVE_FRAGMENT_FORECAST;
    }


    private void showTodayFragment() {
        if (activeFragmentIndex == ACTIVE_FRAGMENT_TODAY)
            return;

        setActiveFragmentIndex(TodayFragment.newInstance());
        activeFragmentIndex = ACTIVE_FRAGMENT_TODAY;
    }


    private void setActiveFragmentIndex(BaseFragment fragment) {

        //TODO: remove stateful layout animation and add custom fragment animation
        // .setCustomAnimations(R.anim.slide_in_top, R.anim.fade_out)

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
        activeFragment = fragment;
    }


    @Override
    public void onLocationChanged(Location location) {
        getSharedPreferences(getPackageName(), MODE_PRIVATE).edit()
                .putBoolean(LocationService.KEY_LOCATION_KNOWN, true)
                .putFloat(LocationService.KEY_LATITUDE, (float) location.getLatitude())
                .putFloat(LocationService.KEY_LONGITUDE, (float) location.getLongitude())
                .putLong(KEY_LOCATION_TIME, location.getTime())
                .apply();

        LocationService.removeLocationListener(this, this);
        ((BaseViewModel) activeFragment.getViewModel()).updateViewModel();
        swipeRefreshLayout.setRefreshing(false);
    }
}
