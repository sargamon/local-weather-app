package com.roncka.weather.android.utility;


/**
 * Author:          Harry
 * Date:            3/25/2017
 * Description:     Utility methods related to weather
 */
public class WeatherUtility {

    public static String getDirection(float deg) {

        if (deg > 337.5 || deg < 22.5)
            return "N";
        else if (deg < 67.5)
            return "NE";
        else if (deg < 112.5)
            return "E";
        else if (deg < 157.5)
            return "SE";
        else if (deg < 202.5)
            return "S";
        else if (deg < 247.5)
            return "SW";
        else if (deg < 292.5)
            return "W";
        else
            return "NW";
    }
}
