package com.roncka.weather.android.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;

import com.roncka.weather.android.R;
import com.roncka.weather.android.databinding.FragmentTodayBinding;
import com.roncka.weather.android.view.TodayView;
import com.roncka.weather.android.viewmodel.TodayViewModel;


/**
 * Author:          Harry
 * Date:   			3/24/2017
 * Inspiration: 	https://github.com/petrnohejl/Android-Stocks/blob/master/mobile/src/main/java/com/example/fragment/StockListMultiFragment.java
 */
public class TodayFragment extends BaseFragment<TodayView, TodayViewModel, FragmentTodayBinding> implements TodayView {

    public static TodayFragment newInstance() {
        return new TodayFragment();
    }


    @Override
    public FragmentTodayBinding inflateBindingLayout(LayoutInflater inflater) {
        return FragmentTodayBinding.inflate(inflater);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(getString(R.string.title_fragment_today));
        setModelView(this);
    }


    @Nullable
    @Override
    public Class<TodayViewModel> getViewModelClass() {
        return TodayViewModel.class;
    }
}
