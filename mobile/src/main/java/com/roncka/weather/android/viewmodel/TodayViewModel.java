package com.roncka.weather.android.viewmodel;

import android.content.Context;
import android.databinding.ObservableField;

import com.roncka.weather.android.LocalWeatherApplication;
import com.roncka.weather.android.LocalWeatherConfig;
import com.roncka.weather.android.entity.WeatherEntity;
import com.roncka.weather.android.rest.dto.CurrentWeatherDTO;
import com.roncka.weather.android.rest.provider.ForecastServiceProvider;
import com.roncka.weather.android.service.LocationService;
import com.roncka.weather.android.utility.WeatherUtility;
import com.roncka.weather.android.view.TodayView;

import org.alfonz.utility.NetworkUtility;
import org.alfonz.view.StatefulLayout;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Author:          Harry
 * Date:   			3/24/2017
 * Inspiration: 	https://github.com/petrnohejl/Android-Stocks/blob/master/mobile/src/main/java/com/example/viewmodel/StockDetailViewModel.java
 */
public class TodayViewModel extends BaseViewModel<TodayView> {

	public final ObservableField<StatefulLayout.State> state = new ObservableField<>();
	public final ObservableField<WeatherEntity> forecast = new ObservableField<>();

    private int TemperatureUnit = com.roncka.weather.android.utility.TemperatureUnit.CELSIUS_CODE;
    private int LengthUnit = com.roncka.weather.android.utility.LengthUnit.METRIC_CODE;


	@Override
	public void onStart() {
		super.onStart();

        if (forecast.get() == null)
			loadData();
        else
            ensureUnits();
    }


    @Override
    public void updateViewModel() {
        loadData();
    }


    private void ensureUnits() {
        if (TemperatureUnit != com.roncka.weather.android.utility.TemperatureUnit.getUnit() || LengthUnit != com.roncka.weather.android.utility.LengthUnit.getUnit()) {
            TemperatureUnit = com.roncka.weather.android.utility.TemperatureUnit.getUnit();
            LengthUnit = com.roncka.weather.android.utility.LengthUnit.getUnit();

            WeatherEntity weather = this.forecast.get();
            setUnitDependentValues(weather);
        }
    }


    private void loadData() {
        Context context = LocalWeatherApplication.getContext();

        if (forecast.get() == null) state.set(StatefulLayout.State.PROGRESS);

        if(NetworkUtility.isOnline(LocalWeatherApplication.getContext())) {

            Call<CurrentWeatherDTO> today;
            if (context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE).getBoolean(LocationService.KEY_LOCATION_KNOWN, false)) {
                float latitude = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE).getFloat(LocationService.KEY_LATITUDE, 0);
                float longitude = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE).getFloat(LocationService.KEY_LONGITUDE, 0);
                today = ForecastServiceProvider.getService().weather(latitude, longitude);
            } else {
                today = ForecastServiceProvider.getService().weather("Brno");
            }

            today.enqueue(new Callback<CurrentWeatherDTO>() {
                @Override
                public void onResponse(Call<CurrentWeatherDTO> call, Response<CurrentWeatherDTO> response) {
                    forecast.set(createWeatherModel(response.body()));
                    state.set(StatefulLayout.State.CONTENT);
                }

                @Override
                public void onFailure(Call<CurrentWeatherDTO> call, Throwable t) {
                    if (forecast.get() == null) state.set(StatefulLayout.State.OFFLINE);
                }
            });
        } else {
            if (forecast.get() == null) state.set(StatefulLayout.State.OFFLINE);
		}
	}


    private WeatherEntity createWeatherModel(CurrentWeatherDTO currentWeatherDTO) {

        WeatherEntity weather = new WeatherEntity();

        weather.setCurrentWeatherDTO(currentWeatherDTO);
        weather.setCityName(currentWeatherDTO.getCityName());
        weather.setIconUrl(String.format("%s%s.png", LocalWeatherConfig.REST_BASE_RESOURCES_URL, currentWeatherDTO.getIconName()));
        weather.setDescription(currentWeatherDTO.getDescription());

        weather.setHumidity(String.format("%s%%", currentWeatherDTO.getHumidity()));
        weather.setPressure(String.format(Locale.getDefault(), "%.0f hPa", currentWeatherDTO.getPressure()));
        weather.setWindDirection(WeatherUtility.getDirection(currentWeatherDTO.getDirection()));

        setUnitDependentValues(weather);

        return weather;
    }


    private void setUnitDependentValues(WeatherEntity weather) {
        CurrentWeatherDTO currentWeatherDTO = weather.getCurrentWeatherDTO();
        weather.setWindSpeed(String.format("%s/h", com.roncka.weather.android.utility.LengthUnit.getInUnit(currentWeatherDTO.getWindSpeed() * com.roncka.weather.android.utility.LengthUnit.METERS_IN_KM, com.roncka.weather.android.utility.LengthUnit.KM_OR_ML)));
        weather.setTemperature(com.roncka.weather.android.utility.TemperatureUnit.getInUnit(currentWeatherDTO.getTemperature()));

        float precipitation = 0;
        if (currentWeatherDTO.getIsPrecipitation()) precipitation = currentWeatherDTO.getPrecipitation();

        weather.setPrecipitation(com.roncka.weather.android.utility.LengthUnit.getInUnit(precipitation, com.roncka.weather.android.utility.LengthUnit.MM_OR_IN));
        forecast.notifyChange();
    }
}
