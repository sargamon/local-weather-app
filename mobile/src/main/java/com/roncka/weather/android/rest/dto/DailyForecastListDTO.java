package com.roncka.weather.android.rest.dto;

import android.databinding.BaseObservable;

import java.util.List;


/**
 * Author:          Harry
 * Date:            3/25/2017
 * Date:            3/23/2017
 * Description:     This class corresponds to daily forecast data json format from openweathermap.org
 */
@SuppressWarnings({"unused"})
public class DailyForecastListDTO extends BaseObservable {

    private City city;

    private List<ForecastDTO> list;


    public String getCityName() {
        return city.name;
    }


    public List<ForecastDTO> getList() {
        return list;
    }


    private class City {
        private String name;
    }
}
