package com.roncka.weather.android.utility;

import android.preference.PreferenceManager;

import com.roncka.weather.android.LocalWeatherApplication;

import java.util.Locale;


/**
 * Author:          Harry
 * Date:            3/24/2017
 * Description:     Simple class allowing to determine unit system based on locale and preferences.
 *                  this class expects the data to be in Kelvins, possible extension would be
 *                  accepting any specified type.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class TemperatureUnit {
    public static final String KEY_TEMPERATURE_CODE = "temperature_unit_list";
    public static final int KELVIN_CODE = -1;
    public static final int FAHRENHEIT_CODE = 0;
    public static final int CELSIUS_CODE = 1;


    public static void setUnit(int unitCode) {
        PreferenceManager.getDefaultSharedPreferences(LocalWeatherApplication.getContext())
                .edit()
                .putString(KEY_TEMPERATURE_CODE, String.valueOf(unitCode))
                .apply();
    }


    public static int getUnit() {
        return Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(LocalWeatherApplication.getContext())
                .getString(KEY_TEMPERATURE_CODE, String.valueOf(CELSIUS_CODE)));
    }


    public static String getInUnit(float value) {
        switch (getUnit()) {
            case FAHRENHEIT_CODE:
                return String.format(Locale.getDefault(), "%.0f°F", (value - 273.15) * 1.8 + 32);
            case KELVIN_CODE:
                return String.format(Locale.getDefault(), "%.0f K", value);
            default:
                return String.format(Locale.getDefault(), "%.0f°C", value - 273.15);
        }
    }
}