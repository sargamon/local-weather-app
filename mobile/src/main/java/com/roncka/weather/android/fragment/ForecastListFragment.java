package com.roncka.weather.android.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.roncka.weather.android.R;
import com.roncka.weather.android.adapter.ForecastListSimpleAdapter;
import com.roncka.weather.android.databinding.FragmentForecastListBinding;
import com.roncka.weather.android.view.ForecastListView;
import com.roncka.weather.android.viewmodel.ForecastListViewModel;


/**
 * Author:          Harry
 * Date:   			3/24/2017
 * Inspiration: 	https://github.com/petrnohejl/Android-Stocks/blob/master/mobile/src/main/java/com/example/fragment/StockDetailFragment.java
 */
public class ForecastListFragment extends BaseFragment<ForecastListView, ForecastListViewModel, FragmentForecastListBinding> implements ForecastListView {

    private ForecastListSimpleAdapter mAdapter;


    public static ForecastListFragment newInstance() {
        return new ForecastListFragment();
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(getString(R.string.title_fragment_forecast));
        setModelView(this);
    }


    @Nullable
    @Override
    public Class<ForecastListViewModel> getViewModelClass() {
        return ForecastListViewModel.class;
    }


    @Override
    public FragmentForecastListBinding inflateBindingLayout(LayoutInflater inflater) {
        return FragmentForecastListBinding.inflate(inflater);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getBinding().executePendingBindings();
        setupAdapter();
    }


    private void setupAdapter() {
        if (mAdapter == null) {
            mAdapter = new ForecastListSimpleAdapter(this, getViewModel());
            getBinding().forecastListRecycler.setAdapter(mAdapter);

            RecyclerView recyclerView = getBinding().forecastListRecycler;
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), ((LinearLayoutManager) recyclerView.getLayoutManager()).getOrientation());
            recyclerView.addItemDecoration(dividerItemDecoration);
        }
    }
}
