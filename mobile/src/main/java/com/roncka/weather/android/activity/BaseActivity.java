package com.roncka.weather.android.activity;

import android.os.Bundle;

import org.alfonz.mvvm.AlfonzActivity;


/**
 * Author:          Harry
 * Date:            3/24/2017
 * Source:		 	https://github.com/petrnohejl/Android-Stocks/blob/master/mobile/src/main/java/com/example/activity/BaseActivity.java
 */
public abstract class BaseActivity extends AlfonzActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}


	@Override
	public void onStart() {
		super.onStart();
	}


	@Override
	public void onResume() {
		super.onResume();
	}


	@Override
	public void onPause() {
		super.onPause();
	}


	@Override
	public void onStop() {
		super.onStop();
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
	}
}
