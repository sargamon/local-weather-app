package com.roncka.weather.android.rest.dto;

import android.databinding.BaseObservable;

import com.google.gson.annotations.SerializedName;


/**
 * Author:          Harry
 * Date:            3/25/2017
 * Description:     Data Transfer Object for a Weather description
 */
@SuppressWarnings({"unused", "MismatchedReadAndWriteOfArray"})
public class CurrentWeatherDTO extends BaseObservable {

    private long id;

    private String name;

    private int cod;

    @SerializedName("dt")
    private long date;

    @SerializedName("coord")
    private Coordinates coordinates;

    private MainInfo main;

    private WeatherDTO[] weather;

    private Precipitation precipitation;

    private Wind wind;


    public long getDate() {
        return date;
    }


    public float getTemperature() {
        return main.temperature;
    }


    public String getCityName() {
        return name;
    }


    public String getIconName() {
        return weather[0].icon;
    }


    public float getPressure() {
        return main.pressure;
    }


    public float getPrecipitation() {
        return precipitation.value;
    }


    public float getHumidity() {
        return main.humidity;
    }


    public String getDescription() {
        return weather[0].description;
    }


    public float getWindSpeed() {
        return wind.speed;
    }


    public float getDirection() {
        return wind.deg;
    }


    public boolean getIsPrecipitation() {
        return precipitation != null;
    }


    private class Coordinates {
        @SerializedName("lon")
        double longitude;
        @SerializedName("lat")
        double lattitude;
    }


    private class MainInfo {
        @SerializedName("temp")
        float temperature;
        float pressure;
        float humidity;
    }


    private class Wind {
        float deg;
        float speed;
        Direction direction;
    }


    private class Precipitation {
        float value;
    }


    private class Direction {
        String value;
        String code;
    }
}
