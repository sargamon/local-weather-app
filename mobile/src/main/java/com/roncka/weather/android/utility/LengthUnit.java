package com.roncka.weather.android.utility;

import android.preference.PreferenceManager;

import com.roncka.weather.android.LocalWeatherApplication;

import java.util.Locale;


/**
 * Author:          Harry
 * Date:            3/24/2017
 * Description:     Simple class allowing to determine unit system based on locale and preferences,
 *                  this class expects to get the data in meters, possible extension would be
 *                  accepting any specified type.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class LengthUnit {
    public static final String KEY_UNIT_CODE = "length_unit_list";
    public static final int IMPERIAL_CODE = 0;
    public static final int METRIC_CODE = 1;

    public static final int UM_OR_TH = 0;
    public static final int MM_OR_IN = 1;
    public static final int M_OR_FT = 2;
    public static final int M_OR_YD = 3;
    public static final int KM_OR_ML = 5;

    public static final float METERS_IN_KM = 1000;


    public static void setUnit(int unitCode) {
        PreferenceManager.getDefaultSharedPreferences(LocalWeatherApplication.getContext())
                .edit()
                .putString(KEY_UNIT_CODE, String.valueOf(unitCode))
                .apply();
    }


    public static int getUnit() {
        return Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(LocalWeatherApplication.getContext())
                .getString(KEY_UNIT_CODE, String.valueOf(METRIC_CODE)));
    }


    public static String getInUnit(float length, int order) {
        if (IMPERIAL_CODE == getUnit()) {
            switch (order) {
                case UM_OR_TH:
                    return String.format(Locale.getDefault(), "%.0f th", length * 39370.1);
                case MM_OR_IN:
                    return String.format(Locale.getDefault(), "%.0f in", length * 39.3701);
                case M_OR_YD:
                    return String.format(Locale.getDefault(), "%.0f yd", length * 1.09361);
                case KM_OR_ML:
                    return String.format(Locale.getDefault(), "%.0f ml", length * 0.000621371);
                default:
                    return String.format(Locale.getDefault(), "%.0f yd", length * 1.09361);
            }
        } else {
            switch (order) {
                case MM_OR_IN:
                    return String.format(Locale.getDefault(), "%.0f mm", length * 1000);
                case KM_OR_ML:
                    return String.format(Locale.getDefault(), "%.0f km", length * 0.001);
                default:
                    return String.format(Locale.getDefault(), "%.0f m", length);
            }
        }
    }
}