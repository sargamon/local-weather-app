package com.roncka.weather.android.fragment;

import android.content.Context;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.roncka.weather.android.view.BaseView;
import com.roncka.weather.android.viewmodel.BaseViewModel;

import org.alfonz.mvvm.AlfonzBindingFragment;


/**
 * Author:          Harry
 * Date:   			3/24/2017
 * Source:       	https://github.com/petrnohejl/Android-Stocks/blob/master/mobile/src/main/java/com/example/fragment/BaseFragment.java
 */
public abstract class BaseFragment<T extends BaseView, R extends BaseViewModel<T>, B extends ViewDataBinding> extends AlfonzBindingFragment<T, R, B> implements BaseView {

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}


	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}


	@Override
	public void onStart() {
		super.onStart();
	}


	@Override
	public void onResume() {
		super.onResume();
	}


	@Override
	public void onPause() {
		super.onPause();
	}


	@Override
	public void onStop() {
		super.onStop();
	}


	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
	}


	@Override
	public void onDetach() {
		super.onDetach();
	}


	@Override
	public void showToast(@StringRes int stringRes) {
		Toast.makeText(getActivity(), stringRes, Toast.LENGTH_LONG).show();
	}


	@Override
	public void showToast(String message) {
		Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
	}


	@Override
	public void showSnackbar(@StringRes int stringRes) {
		if (getView() != null) {
			Snackbar.make(getView(), stringRes, Snackbar.LENGTH_LONG).show();
		}
	}


	@Override
	public void showSnackbar(String message) {
		if (getView() != null) {
			Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
		}
	}
}
