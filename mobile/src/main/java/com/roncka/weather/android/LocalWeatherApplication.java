package com.roncka.weather.android;

import android.app.Application;
import android.content.Context;

import org.alfonz.utility.Logcat;


/**
 * Author:          Harry
 * Date:   			3/24/2017
 * Inspiration: 	https://github.com/petrnohejl/Android-Stocks/blob/master/mobile/src/main/java/com/example/StocksApplication.java
 */
public class LocalWeatherApplication extends Application {

	private static LocalWeatherApplication sInstance;

	public LocalWeatherApplication() {
		sInstance = this;
	}

	public static Context getContext() {
		return sInstance;
	}


	@Override
	public void onCreate() {
		super.onCreate();

		// force AsyncTask to be initialized in the main thread due to the bug:
		// http://stackoverflow.com/questions/4280330/onpostexecute-not-being-called-in-asynctask-handler-runtime-exception
		try {
			Class.forName("android.os.AsyncTask");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		Logcat.init(LocalWeatherConfig.LOGS, "WEATHER");
	}
}