package com.roncka.weather.android.adapter;

import com.roncka.weather.android.R;
import com.roncka.weather.android.databinding.FragmentForecastListItemBinding;
import com.roncka.weather.android.view.ForecastListView;
import com.roncka.weather.android.viewmodel.ForecastListViewModel;

import org.alfonz.adapter.SimpleDataBoundRecyclerAdapter;


/**
 * Author:          Harry
 * Date:            3/24/2017
 * Inspiration: 	https://github.com/petrnohejl/Android-Stocks/blob/master/mobile/src/main/java/com/example/adapter/StockListMultiAdapter.java
 */
public class ForecastListSimpleAdapter extends SimpleDataBoundRecyclerAdapter<FragmentForecastListItemBinding> {
	public ForecastListSimpleAdapter(ForecastListView view, ForecastListViewModel viewModel) {
		super(R.layout.fragment_forecast_list_item,	view, viewModel.forecastList);
	}
}
