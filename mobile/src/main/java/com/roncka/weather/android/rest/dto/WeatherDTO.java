package com.roncka.weather.android.rest.dto;

import android.databinding.BaseObservable;


/**
 * Author:          Harry
 * Date:            3/25/2017
 * Description:     Data Transfer Object for a weather description
 */
@SuppressWarnings({"unused"})
class WeatherDTO extends BaseObservable {
    long id;
    String main;
    String description;
    String icon;
}