package com.roncka.weather.android.rest.dto;

import android.databinding.BaseObservable;

import com.google.gson.annotations.SerializedName;


/**
 * Author:          Harry
 * Date:            3/25/2017
 * Description:     Data Transfer Object for a single forecast
 */
@SuppressWarnings({"unused"})
public class ForecastDTO extends BaseObservable {

    @SerializedName("dt")
    private long date;

    @SerializedName("temp")
    private Temperature temperature;

    private WeatherDTO[] weather;



    public long getDate() {
        return date;
    }


    public float getTemperature() {
        return temperature.day;
    }


    public String getDescription() {
        return weather[0].description;
    }


    public String getIconName() {
        return weather[0].icon;
    }


    private class Temperature {
        float day;
    }
}
