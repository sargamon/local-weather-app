package com.roncka.weather.android.rest;

import com.roncka.weather.android.LocalWeatherConfig;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Author:          Harry
 * Date:   			3/24/2017
 * Inspiration: 	https://github.com/petrnohejl/Android-Stocks/blob/master/mobile/src/main/java/com/example/rest/RetrofitClient.java
 */
public final class RetrofitClient {

	private static volatile Retrofit sRetrofit;


	private RetrofitClient() {
	}


	private static Retrofit getRetrofit() {
		if (sRetrofit == null) {
			synchronized (RetrofitClient.class) {
				if (sRetrofit == null) {
					sRetrofit = buildRetrofit();
				}
			}
		}
		return sRetrofit;
	}


	public static <T> T createService(Class<T> service) {
		return getRetrofit().create(service);
	}


	private static Retrofit buildRetrofit() {
		Retrofit.Builder builder = new Retrofit.Builder()
				.baseUrl(LocalWeatherConfig.REST_BASE_URL)
				.addConverterFactory(GsonConverterFactory.create());
		return builder.build();
	}
}
