package com.roncka.weather.android.entity;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.roncka.weather.android.BR;
import com.roncka.weather.android.rest.dto.CurrentWeatherDTO;


/**
 * Author:          Harry
 * Date:            3/24/2017
 * Description:     Class representing the extended weather status
 * Inspiration: 	https://github.com/petrnohejl/Android-Stocks/blob/master/mobile/src/main/java/com/example/entity/QuoteEntity.java
 */
public class WeatherEntity extends BaseObservable {

    @Bindable
    private String cityName;

    @Bindable
    private String description;

    @Bindable
    private String temperature;

    @Bindable
    private String precipitation;

    @Bindable
    private String pressure;

    @Bindable
    private String humidity;

    @Bindable
    private String windSpeed;

    @Bindable
    private String windDirection;

    @Bindable
    private String iconUrl;

    @Bindable
    private CurrentWeatherDTO currentWeatherDTO;


    public WeatherEntity() {
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
        notifyPropertyChanged(BR.description);
    }


    public String getCityName() {
        return cityName;
    }


    public void setCityName(String cityName) {
        this.cityName = cityName;
        notifyPropertyChanged(BR.cityName);
    }


    public String getWindSpeed() {
        return windSpeed;
    }


    public void setWindSpeed(String windSpeed) {
        this.windSpeed = windSpeed;
        notifyPropertyChanged(BR.windSpeed);

    }


    public String getWindDirection() {
        return windDirection;
    }

    public void setWindDirection(String windDirection) {
        this.windDirection = windDirection;
        notifyPropertyChanged(BR.windDirection);
    }


    public String getHumidity() {
        return humidity;
    }


    public void setHumidity(String humidity) {
        this.humidity = humidity;
        notifyPropertyChanged(BR.humidity);
    }


    public String getPressure() {
        return pressure;
    }


    public void setPressure(String pressure) {
        this.pressure = pressure;
        notifyPropertyChanged(BR.pressure);
    }


    public String getPrecipitation() {
        return precipitation;
    }


    public void setPrecipitation(String precipitation) {
        this.precipitation = precipitation;
        notifyPropertyChanged(BR.precipitation);
    }


    public String getTemperature() {
        return temperature;
    }


    public void setTemperature(String temperature) {
        this.temperature = temperature;
        notifyPropertyChanged(BR.temperature);
    }


    public String getIconUrl() {
        return iconUrl;
    }


    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
        notifyPropertyChanged(BR.iconUrl);
    }


    public CurrentWeatherDTO getCurrentWeatherDTO() {
        return currentWeatherDTO;
    }


    public void setCurrentWeatherDTO(CurrentWeatherDTO currentWeatherDTO) {
        this.currentWeatherDTO = currentWeatherDTO;
        notifyPropertyChanged(BR.currentWeatherDTO);
    }
}
