package com.roncka.weather.android.entity;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.roncka.weather.android.BR;


/**
 * Author:          Harry
 * Date:            3/24/2017
 * Description:     Class representing the daily forecast
 * Inspiration: 	https://github.com/petrnohejl/Android-Stocks/blob/master/mobile/src/main/java/com/example/entity/QuoteEntity.java
 */
public class ForecastEntity extends BaseObservable {

    @Bindable
    private String description;

    @Bindable
    private String temperature;

    @Bindable
    private float temperatureData;

    @Bindable
    private String iconUrl;


    public ForecastEntity() {
    }


    public float getTemperatureData() {
        return temperatureData;
    }


    public void setTemperatureData(float temperatureData) {
        this.temperatureData = temperatureData;
        notifyPropertyChanged(BR.temperatureData);
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
        notifyPropertyChanged(BR.description);
    }


    public String getTemperature() {
        return temperature;
    }


    public void setTemperature(String temperature) {
        this.temperature = temperature;
        notifyPropertyChanged(BR.temperature);
    }


    public String getIconUrl() {
        return iconUrl;
    }


    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
        notifyPropertyChanged(BR.iconUrl);
    }
}
