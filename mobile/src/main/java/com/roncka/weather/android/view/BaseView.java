package com.roncka.weather.android.view;

import android.support.annotation.StringRes;

import org.alfonz.mvvm.AlfonzView;


/**
 * Author:          Harry
 * Date:   			3/24/2017
 * Inspiration: 	https://github.com/petrnohejl/Android-Stocks/blob/master/mobile/src/main/java/com/example/ui/BaseView.java
 */
@SuppressWarnings("unused")
public interface BaseView extends AlfonzView {
	void showToast(@StringRes int stringRes);
	void showToast(String message);
	void showSnackbar(@StringRes int stringRes);
	void showSnackbar(String message);
}
