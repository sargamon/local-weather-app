package com.roncka.weather.android.service;

import android.Manifest;
import android.app.Activity;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;


/**
 * Author:          Harry
 * Date:            24.3.2016
 * Description:     Location service providing with easy access to location updates
 */
public class LocationService extends IntentService implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    protected static final String TAG = "location-service";

    public static final String SERVICE_NAME = "Location service";
    public static final String LOCATION_ENABLED = "location-enabled";

    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_LOCATION_KNOWN = "location-known";
    public static final String KEY_LOCATION_TIME = "location-time";

    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 100;

    static List<LocationListener> locationListeners = new ArrayList<>();

    static GoogleApiClient googleApiClient;
    static LocationRequest locationRequest;


    public LocationService() {
        super(SERVICE_NAME);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        synchronized (TAG) {
            if (intent.getExtras().getBoolean(LOCATION_ENABLED)) {
                if (googleApiClient != null && googleApiClient.isConnected()) {
                    return;
                }

                buildGoogleApiClient();
                googleApiClient.connect();
            } else {
                stopLocationUpdates();
            }
        }
    }


    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }


    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
    }


    public static void stop() {
        synchronized (TAG) {
            if (googleApiClient != null && googleApiClient.isConnected())
                googleApiClient.disconnect();
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            return;

        locationRequest = new LocationRequest();

        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }


    @Override
    public void onConnectionSuspended(int i) {
        googleApiClient.connect();
    }


    @Override
    public void onLocationChanged(Location location) {
        ListIterator<LocationListener> li = locationListeners.listIterator(locationListeners.size());

        while (li.hasPrevious())
            li.previous().onLocationChanged(location);
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }


    @Override
    public boolean stopService(Intent name) {
        stop();
        return super.stopService(name);
    }


    private static void enableService(Context context, boolean enable) {
        Intent intent = new Intent(context, LocationService.class);
        intent.putExtra(LocationService.LOCATION_ENABLED, enable);
        context.startService(intent);
    }


    public static void addLocationListener(Activity activity, LocationListener listener) {
        synchronized (TAG) {
            if (googleApiClient == null || !googleApiClient.isConnected())
                enableService(activity, true);

            locationListeners.add(listener);
        }
    }


    public static void removeLocationListener(Context context, LocationListener listener) {
        synchronized (TAG) {
            locationListeners.remove(listener);

            if (locationListeners.size() == 0)
                enableService(context, false);
        }
    }


    public static boolean checkPermissions(Context context) {
        return (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }


    public static void requestPermissions(Activity activity, int requestCode) {
        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, requestCode);
    }
}