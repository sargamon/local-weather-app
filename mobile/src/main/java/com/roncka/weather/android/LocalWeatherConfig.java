package com.roncka.weather.android;


/**
 * Author:          Harry
 * Date:   			3/24/2017
 * Inspiration: 	https://github.com/petrnohejl/Android-Stocks/blob/master/mobile/src/main/java/com/example/StocksConfig.java
 */
@SuppressWarnings({"WeakerAccess", "ConstantConditions"})
public class LocalWeatherConfig {
    
    public static final boolean LOGS = BuildConfig.LOGS;
    public static final boolean DEV_ENVIRONMENT = BuildConfig.DEV_ENVIRONMENT;

    public static final String API_KEY = "b77830d2e544c667bb41e0e30203f8a2";
    public static final String REST_BASE_URL_PROD = "http://api.openweathermap.org/data/2.5/";
    public static final String REST_BASE_URL_DEV = "http://api.openweathermap.org/data/2.5/";
    public static final String REST_BASE_RESOURCES_URL = "http://openweathermap.org/img/w/";

    public static final String REST_BASE_URL = LocalWeatherConfig.DEV_ENVIRONMENT ? LocalWeatherConfig.REST_BASE_URL_DEV : LocalWeatherConfig.REST_BASE_URL_PROD;
}
